package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.Comment;
import com.intech.tryhardninja.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
}