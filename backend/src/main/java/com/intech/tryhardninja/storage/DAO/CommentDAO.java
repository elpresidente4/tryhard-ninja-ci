package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Comment;

public interface CommentDAO {
    Iterable<Comment> getComment();
    void insertComment(Comment comment);
}
