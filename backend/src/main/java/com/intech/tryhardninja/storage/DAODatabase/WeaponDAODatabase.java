package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.Weapon;
import com.intech.tryhardninja.storage.DAO.WeaponDAO;
import com.intech.tryhardninja.storage.repositories.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class WeaponDAODatabase implements WeaponDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private WeaponRepository weaponRepository;

    public Iterable<Weapon> getWeapon() {
        return weaponRepository.findAll();
    }
    @Override
    public void insertWeapon(Weapon weapon) {
        weaponRepository.save(weapon);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
