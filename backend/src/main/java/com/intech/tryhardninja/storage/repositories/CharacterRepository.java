package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Integer> {
}
