package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.Guide;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuideRepository extends CrudRepository<Guide, Integer> {
}
