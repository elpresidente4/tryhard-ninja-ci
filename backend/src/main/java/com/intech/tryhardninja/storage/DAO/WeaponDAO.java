package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Weapon;

public interface WeaponDAO {
    Iterable<Weapon> getWeapon();
    void insertWeapon(Weapon weapon);
}
