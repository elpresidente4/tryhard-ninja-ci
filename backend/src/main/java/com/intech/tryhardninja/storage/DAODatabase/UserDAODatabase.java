package com.intech.tryhardninja.storage.DAODatabase;

import com.intech.tryhardninja.entities.User;
import com.intech.tryhardninja.storage.DAO.UserDAO;
import com.intech.tryhardninja.storage.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class UserDAODatabase implements UserDAO {

    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getUser() {
        return userRepository.findAll();
    }
    @Override
    public void insertUser(User user) {
        userRepository.save(user);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public User findUserByEmailPseudo(String email, String pseudo) { return userRepository.findByEmailOrPseudo(email, pseudo); }
    public User findUserByEmailPassword(String email, String password) { return userRepository.findByEmailAndPassword(email, password);}

    public Boolean emailValidity(String email) { return userRepository.existsByEmail(email); }
    public Boolean pseudoValidity(String pseudo) { return userRepository.existsByPseudo(pseudo); }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
