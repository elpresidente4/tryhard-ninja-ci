package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.Game;
import com.intech.tryhardninja.storage.DAO.GameDAO;
import com.intech.tryhardninja.storage.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class GameDAODatabase implements GameDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private GameRepository gameRepository;

    public Iterable<Game> getGame() {
        return gameRepository.findAll();
    }
    @Override
    public void insertGame(Game game) {
        gameRepository.save(game);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
