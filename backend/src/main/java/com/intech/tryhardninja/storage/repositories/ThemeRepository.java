package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.Theme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThemeRepository extends CrudRepository<Theme, Integer> {
}
