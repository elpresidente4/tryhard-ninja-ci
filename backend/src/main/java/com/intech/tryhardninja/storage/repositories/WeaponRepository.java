package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.User;
import com.intech.tryhardninja.entities.Weapon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeaponRepository extends CrudRepository<Weapon, Integer> {
}