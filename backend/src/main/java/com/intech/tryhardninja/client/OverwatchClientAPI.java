package com.intech.tryhardninja.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.tryhardninja.entities.OverwatchData;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class OverwatchClientAPI {
    private OkHttpClient client = new OkHttpClient();
    private Map<String, Object> cacheData = new HashMap();
    private ObjectMapper objectMapper = new ObjectMapper();

    public OverwatchClientAPI() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    public OverwatchData getPlayerInfo(@PathVariable String playerName) throws IOException {
        Request request = new Request.Builder()
                .url("https://ow-api.com/v1/stats/pc/eu/"+playerName+"/complete")

                .build();
        Response response = client.newCall(request).execute();
        OverwatchData mappedOverwatchData = new ObjectMapper().readValue(response.body().string(), OverwatchData.class);

        System.out.println(mappedOverwatchData);
        return mappedOverwatchData;
    }
}
