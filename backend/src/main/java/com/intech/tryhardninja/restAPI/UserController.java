package com.intech.tryhardninja.restAPI;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.tryhardninja.entities.User;
import com.intech.tryhardninja.storage.DAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class UserController {

    //private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UserDAO userDAO;

    @PostMapping(value="/authentication")
    public ResponseEntity<Boolean> authentication(@RequestBody User userInfo, HttpServletRequest request) {
        User storedUser = userDAO.findUserByEmailPassword(userInfo.getEmail(), userInfo.getPassword());
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        Boolean body = false;
        if (request.getSession().getAttribute("authenticated") == null && storedUser != null) {
            httpStatus = HttpStatus.OK;
            body = true;
            request.getSession().setAttribute("authenticated", true);
            System.out.println(request.getSession().getId());
        }
        return new ResponseEntity<Boolean>(body, httpStatus);
    }



    @PostMapping(
            value="/register",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Boolean> inscription(@RequestBody User userInfo, HttpServletRequest request) {

        User existantUser = userDAO.findUserByEmailPseudo(userInfo.getEmail(), userInfo.getPseudo());
        HttpStatus httpStatus = HttpStatus.CONFLICT;
        Boolean body = false;

        if (existantUser == null) { // Si l'utilisateur n'existe pas alors on peut l'ajouter
            userDAO.insertUser(userInfo);
            body = true;
            httpStatus = HttpStatus.CREATED;
            System.out.println(request.getSession().getId());
        }

        return new ResponseEntity<Boolean>(body, httpStatus);
    }

    @GetMapping(value="/emailValidity/{email}")
    public Boolean emailValidity(@PathVariable("email") String email) {
        Boolean validity = userDAO.emailValidity(email);
        System.out.println(!validity);
        return !validity;
    }

    @GetMapping(value="/pseudoValidity/{pseudo}")
    public Boolean pseudoValidity(@PathVariable("pseudo") String pseudo) {
        Boolean validity = userDAO.pseudoValidity(pseudo);
        System.out.println(!validity);
        return !validity;
    }

}
