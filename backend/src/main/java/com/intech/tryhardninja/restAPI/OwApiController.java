package com.intech.tryhardninja.restAPI;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.tryhardninja.client.OverwatchClientAPI;
import com.intech.tryhardninja.entities.OverwatchData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@RestController
public class OwApiController {
    private ObjectMapper objectMapper = new ObjectMapper();
    private OverwatchClientAPI overwatchClientAPI = new OverwatchClientAPI();
    private Object OverwatchData;

    @GetMapping(value="/getOwData/{playerName}")
    public Object getOwData(@PathVariable String playerName) throws IOException {
        OverwatchData mappedOverwatchData = overwatchClientAPI.getPlayerInfo(playerName);
        return mappedOverwatchData;
    }

}
