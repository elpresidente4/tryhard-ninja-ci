package com.intech.tryhardninja.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompetitiveStats {
    private Awards awards;
    private Games games;

    public Games getGames() {
        return games;
    }

    public void setGames(Games games) {
        this.games = games;
    }

    public Object getCareerStats() {
        return careerStats;
    }

    public void setCareerStats(Object careerStats) {
        this.careerStats = careerStats;
    }

    private Object careerStats;

    public Awards getAwards() {
        return awards;
    }

    public void setAwards(Awards awards) {
        this.awards = awards;
    }
}
