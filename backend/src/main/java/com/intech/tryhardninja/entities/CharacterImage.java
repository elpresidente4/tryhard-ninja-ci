package com.intech.tryhardninja.entities;

import javax.persistence.*;

@Entity
@Table(name = "character_image")
public class CharacterImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "path", unique = false)
    private int path;

    public int getPath() {
        return path;
    }

    public void setPath(int path) {
        this.path = path;
    }

    private enum type {
        mainSkin, altSkin
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "character_id")
    private Character character;

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
