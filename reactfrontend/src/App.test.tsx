import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './app/App';

test('renders footers support', () => {
  render(<App />);
  const linkElement = screen.getByText(/SUPPORT/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders footers cookies', () => {
  render(<App />);
  const linkElement = screen.getByText(/COOKIES/i);
  expect(linkElement).toBeInTheDocument();
});
