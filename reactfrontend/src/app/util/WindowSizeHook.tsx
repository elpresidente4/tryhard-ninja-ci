import React from 'react';

const WindowSizeHook = () => 
{
    const [width, setWidth] = React.useState(window.innerWidth);
    const [height, setHeight] = React.useState(window.innerHeight);
      
    React.useEffect(
        () => 
        {
            const handleWindowResize = () => 
            {
                setWidth(window.innerWidth);
                setHeight(window.innerHeight);
            }
            window.addEventListener("resize", handleWindowResize);
            return () => window.removeEventListener("resize", handleWindowResize);
        }, []
    )
      
    return { width, height};
}
export default WindowSizeHook;