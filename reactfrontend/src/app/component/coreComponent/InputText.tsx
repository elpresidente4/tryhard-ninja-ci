import React from 'react';
import './../../../css/CoreComponent.css';

interface Props {
    label?: string;
    type?: string;
    value?: string | number;
}

const InputText: React.FC<React.HTMLAttributes<HTMLInputElement> & Props> = (
    {
        onChange,
        onBlur,
        value,
        label,
        type,
        className,
        placeholder
    }
) => 
{
    if(label)
    {
        return(
        <label>
            {label}
            <input 
                value={value}
                type={type}
                onChange={onChange}
                onBlur={onBlur}
                className={className}
                placeholder={placeholder}
            />
        </label>
    )
    }
    else
    {
        return (
            <input 
                value={value}
                type={type}
                onChange={onChange}
                onBlur={onBlur}
                className={className}
                placeholder={placeholder}
            />
        );
    }

}

export default InputText;