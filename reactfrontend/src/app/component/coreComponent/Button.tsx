import React from "react";
import './../../../css/CoreComponent.css';

interface Props{
    clickable?: boolean
}

const Button: React.FC<React.HTMLAttributes<HTMLButtonElement> & Props> = 
(
    {
        children,
        onClick,
        className,
        clickable
    }
) => 
{
    return(
        <button 
        disabled={clickable}
        className={className}
        onClick={onClick}
        >
            {children}
        </button>
    )
}
export default Button;