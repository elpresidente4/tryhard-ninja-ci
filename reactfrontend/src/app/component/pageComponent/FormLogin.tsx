import React, { useState } from 'react';
import {Button, InputText} from '../coreComponent/Index';
import axios from '../../util/Axios';
import { useHistory } from "react-router-dom";

const FormLogin: React.FC = () => {
	const [email, setEmail] = useState({ text: '', localValidity: true });
	const [pwd, setPwd] = useState({ text: '', localValidity: true });
	const history = useHistory();

	const handleSubmit = async () => 
	{
		console.log("test")
		try 
		{
			const res = await axios.loginReq(email.text, pwd.text);
			if (res) 
			{
				history.push("/");
			} 
			else 
			{
				console.log("Connexion impossible, veuillez réessayer");
			}
		} 
		catch (e) 
		{
			if (e.response) 
			{
				console.log(e.response);
				alert("Email ou mot de passe incorrect(s)");
			}
			else if (e.request) 
			{
				console.log(e.request);
				alert("Pas de réponse du serveur, veuillez réessayer plus tard");
			}
			else 
			{
				console.log(e);
				alert("Erreur inconnue");
			}
		}
	}

	const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setEmail({
			text: e.currentTarget.value,
			localValidity: 	true
		})
	}

	const handlePwdChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setPwd({
			text: e.currentTarget.value,
			localValidity: true
		})
	}

	return (
		<>
			<div className="TittleContainerLogin">
				Connexion
			</div>

			<div className="ContainerLogin">
				<InputText
					className="ImputText"
					placeholder="Email"
					type="text"
					value={email.text}
					onChange={handleEmailChange}
				/>
			</div>

			<div className="ContainerLogin">
				<InputText
					className="ImputText"
					placeholder="Password"
					value={pwd.text}
					type="password"
					onChange={handlePwdChange}
				/>
			</div>

			<div className="ContainerLogin">
				<Button className="ButtonDefault2" onClick={handleSubmit}>Confirmer</Button>
				<Button className="ButtonTransparent2" onClick={() => history.push("/register")}>Créer un compte</Button>
			</div>
		</>
	);
}
export default FormLogin;