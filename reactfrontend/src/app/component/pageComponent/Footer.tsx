import React from 'react';
import {Button} from '../coreComponent/Index';
import './../../../css/Footer.css';

interface Props{}

const Footer: React.FC<Props> = () => 
{
    return(
        <div className="Footer">

            {/* Project column */}
            <div className="ProjectFooter">
                
                {/* Logo */}
                <div className="LogoContainerFooter">
                    <img className="LogoFooter" src="./image/Logo.png" alt="logo du site tryhard ninja"/>
                </div>

                {/* Project description */}
                <span className="DescriptionContainerFooter">
                    Aenean semper sapien arcu, ut sodales dolor elementum ut. Vivamus at
                    neque elit. Nulla vel lorem vitae odio ullamcorper accumsan ac non
                    risus. Sed mollis, nisl nec placerat blandit, erat urna blandit dui,
                    in hendrerit tellus quam id arcu. Aenean lobortis bibendum nulla,
                    nec tincidunt mi accumsan ultricies. Vivamus posuere tincidunt nisi
                </span>
                <span className="DescriptionContainerFooter">
                    Aenean semper sapien arcu, ut sodales dolor elementum ut. Vivamus at
                    neque elit. Nulla vel lorem vitae odio ullamcorper accumsan ac non
                    risus. Sed mollis, nisl nec placerat blandit, erat urna blandit dui,
                    in hendrerit tellus quam id arcu. Aenean lobortis bibendum nulla,
                    nec tincidunt mi accumsan ultricies. Vivamus posuere tincidunt nisi
                </span>

            </div>

            {/* Mobile application column */}
            <div className="MobileApplicationFooter">

                {/* Column title */}
                <span className="TittleContainerFooter">
                    TELECHARGER NOTRE APPLICATION
                </span>

                {/* IOS application button */}
                <div className="AppLogoContainerFooter">
                    <Button className="ButtonImage">
                        <img className="AppLogoFooter" src="./image/AppleStore.svg" alt="logo de l'applestore" />
                    </Button>
                </div>

                {/* Android appication button */}
                <div className="AppLogoContainerFooter">
                    <Button className="ButtonImage">
                        <img className="AppLogoFooter" src="./image/PlayStore.svg" alt="logo du playstore" />
                    </Button>
                </div>

            </div>

            {/* Support column */}
            <div className="SupportFooter">

                {/* Column title */}
                <span className="TittleContainerFooter">
                    SUPPORT
                </span>

                {/* About us button */}
                <div className="ButtonContainerFooter">
                    <Button className="ButtonTransparentLightBlue">
                        A PROPOS DE NOUS
                    </Button>
                </div>

                {/* Contact button */}
                <div className="ButtonContainerFooter">
                    <Button className="ButtonTransparentLightBlue">
                        NOUS CONTACTER
                    </Button>
                </div>

                {/* Help button */}
                <div className="ButtonContainerFooter">
                    <Button className="ButtonTransparentLightBlue">
                        AIDE
                    </Button>
                </div>

                {/* Cookies button */}
                <div className="ButtonContainerFooter">
                    <Button className="ButtonTransparentLightBlue">
                        COOKIES
                    </Button>
                </div>

                {/* FAQ button */}
                <div className="ButtonContainerFooter">
                    <Button className="ButtonTransparentLightBlue">
                        FAQ
                    </Button>
                </div>

            </div>

        </div>
    )
    
}

export default Footer;